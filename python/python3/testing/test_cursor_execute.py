# pymysql

@patch('module.pymysql.connect')
def test_execute_query(self, mock_pymysql):
    mock_execute = MagicMock()
    mock_cursor = MagicMock()
    mock_cursor.cursor().__enter__().execute = mock_execute
    mock_pymysql.return_value = mock_cursor

    query = 'foo'
    self.program.connect_database()
    self.program.execute_query(query)
    mock_execute.assert_called_once_with(query)
