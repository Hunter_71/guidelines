#!/usr/bin/env python3.4

PL_TO_HTML = {
    'Ą': '&Aogon;',
    'ą': '&aogon;',
    'Ć': '&Cacute;',
    'ć': '&cacute;',
    'Ę': '&Eogon;',
    'ę': '&eogon;',
    'Ł': '&Lstrok;',
    'ł': '&lstrok;',
    'Ń': '&Nacute;',
    'ń': '&nacute;',
    'Ó': '&Oacute;',
    'ó': '&oacute;',
    'Ś': '&Sacute;',
    'ś': '&sacute;',
    'Ź': '&Zacute;',
    'ź': '&zacute;',
    'Ż': '&Zdot;',
    'ż': '&zdot;',
}

HTML_TO_PL = {v: k for k, v in PL_TO_HTML.items()}
HTML_TO_PL.update({
    '&#260;': 'Ą',
    '&#261;': 'ą',
    '&#262;': 'Ć',
    '&#263;': 'ć',
    '&#280;': 'Ę',
    '&#281;': 'ę',
    '&#321;': 'Ł',
    '&#322;': 'ł',
    '&#323;': 'Ń',
    '&#324;': 'ń',
    '&#211;': 'Ó',
    '&#243;': 'ó',
    '&#346;': 'Ś',
    '&#347;': 'ś',
    '&#377;': 'Ź',
    '&#378;': 'ź',
    '&#379;': 'Ż',
    '&#380;': 'ż',
})

PL_TO_UNICODE = {
    'Ą': 'U+0104',
    'ą': 'U+0105',
    'Ć': 'U+0106',
    'ć': 'U+0107',
    'Ę': 'U+0118',
    'ę': 'U+0119',
    'Ł': 'U+0141',
    'ł': 'U+0142',
    'Ń': 'U+0143',
    'ń': 'U+0144',
    'Ó': 'U+00D3',
    'ó': 'U+00F3',
    'Ś': 'U+015A',
    'ś': 'U+015B',
    'Ź': 'U+0179',
    'ź': 'U+017A',
    'Ż': 'U+017B',
    'ż': 'U+017C',
}

UNICODE_TO_PL = {v: k for k, v in PL_TO_UNICODE.items()}


def pl2html(string):
    return convert(string, PL_TO_HTML)


def html2pl(string):
    return convert(string, HTML_TO_PL)


def pl2unicode(string):
    return convert(string, PL_TO_UNICODE)


def unicode2pl(string):
    return convert(string, UNICODE_TO_PL)


def convert(string, replacements_dict):
    result = string

    for pattern, replacement in replacements_dict.items():
        result = result.replace(pattern, replacement)

    return result