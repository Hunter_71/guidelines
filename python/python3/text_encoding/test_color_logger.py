#!/usr/bin/env python3.4

import logging
import mock
import unittest

from loggers.color_logger import ColorLogger


class TestColorLogger(unittest.TestCase):

    def setUp(self):
        self.logger = ColorLogger(filename='', log_console=False)

    # mocking pathlib prevents creating log_file by ColorLogger class
    @mock.patch('loggers.color_logger.pathlib')
    def test_init(self, mock_pathlib):
        logging.getLogger = mock.MagicMock('getLogger')
        logging.FileHandler = mock.MagicMock('FileHandler')
        logging.StreamHandler = mock.MagicMock('StreamHandler')

        ColorLogger(filename='', log_console=False)

        self.assertTrue(logging.getLogger.called)
        self.assertEqual(logging.getLogger.call_count, 2)
        self.assertFalse(logging.FileHandler.called)
        self.assertFalse(logging.StreamHandler.called)

        logging.FileHandler.reset_mock()
        logging.StreamHandler.reset_mock()

        ColorLogger(filename='', log_console=True)

        self.assertFalse(logging.FileHandler.called)
        self.assertTrue(logging.StreamHandler.called)
        self.assertEqual(logging.StreamHandler.call_count, 1)

        logging.FileHandler.reset_mock()
        logging.StreamHandler.reset_mock()

        ColorLogger(filename='foo', log_console=False)

        self.assertTrue(logging.FileHandler.called)
        self.assertEqual(logging.FileHandler.call_count, 1)
        self.assertFalse(logging.StreamHandler.called)

        logging.FileHandler.reset_mock()
        logging.StreamHandler.reset_mock()

        ColorLogger(filename='foo', log_console=True)

        self.assertTrue(logging.FileHandler.called)
        self.assertEqual(logging.StreamHandler.call_count, 1)
        self.assertTrue(logging.StreamHandler.called)
        self.assertEqual(logging.StreamHandler.call_count, 1)

    def test_critical(self):
        message = 'foo'

        self.logger.file_logger.critical = mock.MagicMock()
        self.logger.console_logger.critical = mock.MagicMock()
        self.logger.root.critical = mock.MagicMock()

        self.logger.file_logger.critical(message)
        self.logger.console_logger.critical(message)
        self.logger.root.critical(message)

        self.logger.file_logger.critical.assert_called_once_with(message)
        self.logger.console_logger.critical.assert_called_once_with(message)
        self.logger.root.critical.assert_called_once_with(message)

    def test_debug(self):
        message = 'foo'

        self.logger.file_logger.debug = mock.MagicMock()
        self.logger.console_logger.debug = mock.MagicMock()
        self.logger.root.debug = mock.MagicMock()

        self.logger.file_logger.debug(message)
        self.logger.console_logger.debug(message)
        self.logger.root.debug(message)

        self.logger.file_logger.debug.assert_called_once_with(message)
        self.logger.console_logger.debug.assert_called_once_with(message)
        self.logger.root.debug.assert_called_once_with(message)

    def test_error(self):
        message = 'foo'

        self.logger.file_logger.error = mock.MagicMock()
        self.logger.console_logger.error = mock.MagicMock()
        self.logger.root.error = mock.MagicMock()

        self.logger.file_logger.error(message)
        self.logger.console_logger.error(message)
        self.logger.root.error(message)

        self.logger.file_logger.error.assert_called_once_with(message)
        self.logger.console_logger.error.assert_called_once_with(message)
        self.logger.root.error.assert_called_once_with(message)

    def test_exception(self):
        message = 'foo'

        self.logger.file_logger.exception = mock.MagicMock()
        self.logger.console_logger.exception = mock.MagicMock()
        self.logger.root.exception = mock.MagicMock()

        self.logger.file_logger.exception(message)
        self.logger.console_logger.exception(message)
        self.logger.root.exception(message)

        self.logger.file_logger.exception.assert_called_once_with(message)
        self.logger.console_logger.exception.assert_called_once_with(message)
        self.logger.root.exception.assert_called_once_with(message)

    def test_fatal(self):
        message = 'foo'

        self.logger.file_logger.fatal = mock.MagicMock()
        self.logger.console_logger.fatal = mock.MagicMock()
        self.logger.root.fatal = mock.MagicMock()

        self.logger.file_logger.fatal(message)
        self.logger.console_logger.fatal(message)
        self.logger.root.fatal(message)

        self.logger.file_logger.fatal.assert_called_once_with(message)
        self.logger.console_logger.fatal.assert_called_once_with(message)
        self.logger.root.fatal.assert_called_once_with(message)

    def test_warning(self):
        message = 'foo'

        self.logger.file_logger.warning = mock.MagicMock()
        self.logger.console_logger.warning = mock.MagicMock()
        self.logger.root.warning = mock.MagicMock()

        self.logger.file_logger.warning(message)
        self.logger.console_logger.warning(message)
        self.logger.root.warning(message)

        self.logger.file_logger.warning.assert_called_with(message)
        self.logger.console_logger.warning.assert_called_with(message)
        self.logger.root.warning.assert_called_with(message)

    def test_info(self):
        message = 'foo'

        self.logger.file_logger.info = mock.MagicMock()
        self.logger.console_logger.info = mock.MagicMock()
        self.logger.root.info = mock.MagicMock()

        self.logger.file_logger.info(message)
        self.logger.console_logger.info(message)
        self.logger.root.info(message)

        self.logger.file_logger.info.assert_called_once_with(message)
        self.logger.console_logger.info.assert_called_once_with(message)
        self.logger.root.info.assert_called_with(message)

    def test_info_red(self):
        message = 'foo'

        self.logger.file_logger.info = mock.MagicMock()
        self.logger.console_logger.info = mock.MagicMock()
        self.logger.root.info = mock.MagicMock()

        self.logger.info_red(message)

        self.logger.file_logger.info.assert_called_once_with(message)
        self.assertTrue(self.logger.console_logger.info.called)
        self.assertEqual(self.logger.console_logger.info.call_count, 1)
        self.assertFalse(self.logger.root.info.called)

    def test_info_green(self):
        message = 'foo'

        self.logger.file_logger.info = mock.MagicMock()
        self.logger.console_logger.info = mock.MagicMock()
        self.logger.root.info = mock.MagicMock()

        self.logger.info_green(message)

        self.logger.file_logger.info.assert_called_once_with(message)
        self.assertTrue(self.logger.console_logger.info.called)
        self.assertEqual(self.logger.console_logger.info.call_count, 1)
        self.assertFalse(self.logger.root.info.called)

    def test_info_yellow(self):
        message = 'foo'

        self.logger.file_logger.info = mock.MagicMock()
        self.logger.console_logger.info = mock.MagicMock()
        self.logger.root.info = mock.MagicMock()

        self.logger.info_yellow(message)

        self.logger.file_logger.info.assert_called_once_with(message)
        self.assertTrue(self.logger.console_logger.info.called)
        self.assertEqual(self.logger.console_logger.info.call_count, 1)
        self.assertFalse(self.logger.root.info.called)


if __name__ == '__main__':
    unittest.main()
