#!/usr/bin/env python3.4

import logging
import pathlib


class ColorLogger(logging.getLoggerClass()):
    LOGGING_INFO_RED = '\033[0;31m%s\033[0m'
    LOGGING_INFO_GREEN = '\033[0;32m%s\033[0m'
    LOGGING_INFO_YELLOW = '\033[0;33m%s\033[0m'

    LOGGER_FILE = 'file'
    LOGGER_CONSOLE = 'console'

    DEFAULT_FORMAT = {
        'fmt': '%(asctime)s %(levelname)s: %(message)s',
        'datefmt': '%Y-%m-%d %H:%M:%S',
    }

    def __init__(self, filename='', log_console=False, format=DEFAULT_FORMAT):
        formatter = logging.Formatter(**format)

        self.root.setLevel(logging.INFO)
        self.root.handlers = []

        self.file_logger = logging.getLogger('file_logger')
        self.file_logger.setLevel(logging.INFO)
        self.file_logger.handlers = []

        self.console_logger = logging.getLogger('console_logger')
        self.console_logger.setLevel(logging.INFO)
        self.console_logger.handlers = []

        if filename:
            log_file = pathlib.Path(filename)

            if not log_file.parent.exists():
                log_file.parent.mkdir(parents=True)
            if not log_file.exists():
                log_file.touch()

            handler = logging.FileHandler(str(log_file))
            handler.setFormatter(formatter)
            self.file_logger.addHandler(handler)

        if log_console:
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            self.console_logger.addHandler(handler)

    def critical(self, message):
        self.file_logger.critical(message)
        self.console_logger.critical(message)

    def debug(self, message):
        self.file_logger.debug(message)
        self.console_logger.debug(message)

    def error(self, message):
        self.file_logger.error(message)
        self.console_logger.error(message)

    def exception(self, message):
        self.file_logger.exception(message)
        self.console_logger.exception(message)

    def fatal(self, message):
        self.file_logger.fatal(message)
        self.console_logger.fatal(message)

    def warning(self, message):
        self.file_logger.warning(message)
        self.console_logger.warning(message)

    def info(self, message):
        self.file_logger.info(message)
        self.console_logger.info(message)

    def info_red(self, message):
        self.file_logger.info(message)
        self.console_logger.info(self.LOGGING_INFO_RED, message)

    def info_green(self, message):
        self.file_logger.info(message)
        self.console_logger.info(self.LOGGING_INFO_GREEN, message)

    def info_yellow(self, message):
        self.file_logger.info(message)
        self.console_logger.info(self.LOGGING_INFO_YELLOW, message)
