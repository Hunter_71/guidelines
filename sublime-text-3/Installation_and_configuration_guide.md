# Sublime Text 3 installation and configuration guide

## Installation

### Install Sublime Text 3 editor

Type below commands in terminal:
```bash
$ sudo add-apt-repository ppa:webupd8team/sublime-text-3
$ sudo apt-get update
$ sudo apt-get install sublime-text-installer
```
Then open the **Sublime Text 3** editor and press `ctrl + ~` combination to see *Package control* frame. Paste there below command list and press `enter`:
```
import urllib.request,os,hashlib; h = 'eb2297e1a458f27d836c04bb0cbaf282' + 'd0e7a3098092775ccb37ca9d6b2e4b7d'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```

### Install Sublime packages

Press `ctrl + shift + p` combination to see *Command Palette* and type "Install Packages" then press `enter` on selected label. Now you should be able to see new frame with listed Sublime packages. We recommend to install below packages:
* **SideBarEnchancement**  
    *Enlarge number of operations allowed with files and folders.*  
* **SublimeCodeIntel**  
    *Intelligent type hints in real-time.*  
* **Anaconda**  
    *Powerful IDE for Python in Sublime Text 3 environment.*  
* **Python 3, PythonTest**  
* **Djaneiro**  
    *Supports Django framework in Sublime Text 3 environment.*  
* **Git, GitConfig, GitGutter**  
    *Tools supporting Git CVS syntax, Git configuration files syntax and visualization of Git repository changes with simple pictograms in Sublime Text 3 environment.*  
* **GitlabIntegrate**  
    *Supports integration of Sublime projects with Gitlab.*  
* **FTPSync**  
    *Supports FTP data synchronization with Sublime projects.*    
* **Markdown Preview**  
    *Supports Markdown in Sublime Text 3 environment.*  
* **SublimeLinter**  
    *Tool supporting lints with multiple set of programming languages (ex. python, cpp, html, css, etc...).*  
  * SublimeLinter-csslint
  * SublimeLinter-cpplint
  * SublimeLinter-cpplint
  * SublimeLinter-xmllint
  * SublimeLinter-pyyaml
  * SublimeLinter-contrib-htmlhint
  * SublimeLinter-contrib-sqlint

## Configuration

### Sublime Text 3 configuration

Sample `Settings - User` configuration of **Sublime Text 3** editor which can be found at *Preferences* menu tab:  
[Sublime Text 3 User configuration](https://gitlab.com/Hunter_71/guidelines/blob/master/sublime-text-3/Sublime_configuration.json)

### Anaconda configuration

Sample `Settings - User` configuration of **Anaconda IDE** which can be found at *Preferences -> Package Settings -> Anaconda* menu tab:  
[Anaconda User configuration](https://gitlab.com/Hunter_71/guidelines/blob/master/sublime-text-3/Anaconda_configuration.json)

## Authors

**Tomasz Zięba**  
Gitlab: [@Hunter_71](https://gitlab.com/u/Hunter_71)  
Bitbucket: [@Hunter_71](https://bitbucket.org/Hunter_71/)  
Email: *tomasz.zieba.71@gmail.com*  
Skype: *tomasz.zieba.71*  
Site: [Stowarzyszenie "Walka Duchowa"](http://walkaduchowa.pl/)  

## Bibliography

1. [Sublime](http://www.sublimetext.com/3)
1. [Install Sublime Text 3 on Ubuntu](http://askubuntu.com/questions/172698/how-do-i-install-sublime-text-2-3)
1. [Anaconda](http://damnwidget.github.io/anaconda/)
1. [Configure Sublime Text 3 as Python IDE](https://realpython.com/blog/python/setting-up-sublime-text-3-for-full-stack-python-development/)
1. [Markdown syntax](https://gitlab.com/help/markdown/markdown)
1. [Dillinger - markdown preview online](http://dillinger.io/)