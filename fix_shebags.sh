#!/usr/bin/bash

PY_SHEBANG="#!/usr/bin/python"
PY_SHEBANG_ESCAPED="$(echo $PY_SHEBANG | sed 's/\//\\\//g')"
SHEBANG_PATTERN="^#\!.*python$"

for f in $(grep -rl $SHEBANG_PATTERN ..)
do
    if [ -f "$f" ] && [ ! -L "$f" ]; then
        first_line="$(head -n 1 $f)"

        if [ "$first_line" != "$PY_SHEBANG" ]; then
            $(sed -i "1s/^.*$/$PY_SHEBANG_ESCAPED/" $f)
        fi
    fi
done
